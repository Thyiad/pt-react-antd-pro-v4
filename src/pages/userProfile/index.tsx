import { DownOutlined, PlusOutlined } from '@ant-design/icons';
import {
  Form,
  Button,
  DatePicker,
  Input,
  Modal,
  Radio,
  Select,
  Steps,
  Cascader,
  Switch,
  Avatar,
} from 'antd';
import UploadBtn from '@/components/Upload/UploadBtnFC';
import React, { useState, useRef, useEffect } from 'react';
import { Dispatch } from 'redux';
import { RouteChildrenProps } from 'react-router';
import { connect } from 'dva';
import { ConnectProps, ConnectState } from '@/models/connect';
import { CurrentUser } from '@/models/user';
import { sexList } from '@/constants/selectMaps';
import { roles, defaultAvatarUrl } from '@/constants/index';

import { toast } from '@/utils/utils';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import styles from './index.less';

const FormItem = Form.Item;
const { Option } = Select;

interface ProfileProps extends RouteChildrenProps {
  dispatch: Dispatch<any>;
  currentUser?: Partial<CurrentUser>;
  loading?: boolean;
}

const Profile: React.FC<ProfileProps> = props => {
  const { currentUser, dispatch, loading } = props;
  const [form] = Form.useForm();
  const [avatarUrl, setAvatarUrl] = useState<string | undefined>(defaultAvatarUrl);

  useEffect(() => {
    form.resetFields();
    // @ts-ignore
    form.setFieldsValue(currentUser);
  }, [form, currentUser]);

  const formFinish = (values: CurrentUser) => {
    dispatch({
      type: 'user/updateProfile',
      payload: {
        ...values,
        avatar: avatarUrl,
      },
    })
      // @ts-ignore
      .then(() => {
        toast('更新成功');
      });
  };

  const handleOk = () => {
    form.validateFields().then(formFinish);
  };

  return (
    <PageHeaderWrapper>
      <div className={styles.profileContainer}>
        <Form layout="vertical" form={form} onFinish={formFinish} className={styles.form}>
          {/* account */}
          <FormItem
            name="account"
            label="账号"
            rules={[{ required: true, message: '请输入账号!' }]}
          >
            <Input disabled placeholder="请输入账号" />
          </FormItem>
          {/* name */}
          <FormItem name="name" label="姓名" rules={[{ required: true, message: '请输入姓名' }]}>
            <Input placeholder="请输入姓名" />
          </FormItem>
          {/* avatar */}
          <FormItem name="avatar" label="头像">
            <Avatar src={avatarUrl} style={{ marginRight: 8 }} />
            <UploadBtn
              uploadParams={{ saveDir: 'opoc/userAvatar/', keepName: false }}
              uploadSuc={sucUrl => setAvatarUrl(sucUrl)}
              fileSize={2}
              extList={['.png', '.jpg', '.jpeg', '.gif']}
            />
          </FormItem>
          {/* sex */}
          <FormItem name="sex" label="性别" rules={[{ required: true, message: '请选择性别' }]}>
            <Select allowClear placeholder="请选择性别">
              {sexList.map(item => (
                <Option key={item.value} value={item.value}>
                  {item.label}
                </Option>
              ))}
            </Select>
          </FormItem>
          {/* phone */}
          <FormItem
            name="phone"
            label="手机号码"
            rules={[
              { required: true, message: '请输入手机号码' },
              { len: 11, message: '请输入合法的11位手机号码' },
            ]}
          >
            <Input placeholder="请输入手机号码" />
          </FormItem>
          {/* email */}
          <FormItem
            name="email"
            label="邮箱"
            rules={[{ pattern: /^[^@]+@[^@]+$/, message: '请输入合法的邮箱' }]}
          >
            <Input placeholder="请输入邮箱" />
          </FormItem>
          {/* desc */}
          <FormItem name="desc" label="描述">
            <Input.TextArea placeholder="请输入描述" />
          </FormItem>
        </Form>
        <Button loading={loading} type="primary" onClick={handleOk}>
          更新基本信息
        </Button>
      </div>
    </PageHeaderWrapper>
  );
};

export default connect(({ user, loading }: ConnectState) => ({
  loading: loading.effects['user/updateProfile'],
  currentUser: user.currentUser,
}))(Profile);
