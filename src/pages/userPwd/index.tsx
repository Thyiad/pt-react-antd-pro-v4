import { DownOutlined, PlusOutlined } from '@ant-design/icons';
import {
  Form,
  Button,
  DatePicker,
  Input,
  Modal,
  Radio,
  Select,
  Steps,
  Cascader,
  Switch,
  Avatar,
} from 'antd';
import React, { useState, useRef, useEffect } from 'react';
import { RouteChildrenProps } from 'react-router';
import { toast } from '@/utils/utils';

import { post } from '@/utils/request';
import * as api from '@/constants/api';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const FormItem = Form.Item;

interface FormValues {
  oldPassword?: string;
  newPassword?: string;
  newPasswordConfirm?: string;
}

const UpdatePwd: React.FC<RouteChildrenProps> = props => {
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  const formFinish = (values: FormValues) => {
    if (values.newPasswordConfirm !== values.newPassword) {
      toast('新密码与确认密码不一致');
      return;
    }

    setLoading(true);
    post(api.UPDATE_PASSWORD, values)
      .then(res => {
        toast('修改成功');
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
      });
  };

  const handleOk = () => {
    form.validateFields().then(formFinish);
  };

  return (
    <PageHeaderWrapper>
      <div style={{ backgroundColor: 'white', padding: '24px 72px' }}>
        <Form layout="vertical" form={form} onFinish={formFinish} style={{ maxWidth: '360px' }}>
          <FormItem
            name="oldPassword"
            label="原密码"
            rules={[{ required: true, message: '请输入原密码' }]}
          >
            <Input.Password placeholder="请输入原密码" />
          </FormItem>
          <FormItem
            name="newPassword"
            label="新密码"
            rules={[{ required: true, message: '请输入新密码' }]}
          >
            <Input.Password placeholder="请输入新密码" />
          </FormItem>
          <FormItem
            name="newPasswordConfirm"
            label="确认密码"
            rules={[{ required: true, message: '请输入确认密码' }]}
          >
            <Input.Password placeholder="请输入确认密码" />
          </FormItem>
        </Form>
        <Button loading={loading} type="primary" onClick={handleOk}>
          修改密码
        </Button>
      </div>
    </PageHeaderWrapper>
  );
};

export default UpdatePwd;
