import React from 'react';
import { Dispatch } from 'redux';
import { RouteChildrenProps } from 'react-router';
import { connect } from 'dva';
import { ConnectState } from '@/models/connect';
import { Skeleton, Avatar, Card } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

import moment from 'moment';
import { getTimeNameFromHour } from '@/utils/utils';
import { CurrentUser } from '@/models/user';
import styles from './Welcome.less';

interface WelcomeProps extends RouteChildrenProps {
  dispatch: Dispatch<any>;
  currentUser?: CurrentUser;
}

const PageHeaderContent: React.FC<{ currentUser?: CurrentUser }> = ({ currentUser }) => {
  const loading = currentUser && Object.keys(currentUser).length;
  if (!loading) {
    return <Skeleton avatar paragraph={{ rows: 1 }} active />;
  }

  return (
    <div className={styles.pageHeaderContent}>
      <div className={styles.avatar}>
        <Avatar size="large" src={currentUser?.avatar} />
      </div>
      <div className={styles.content}>
        <div className={styles.contentTitle}>
          {getTimeNameFromHour(moment().hour())}好，
          {currentUser?.name}
          ，祝你开心每一天！
        </div>
        <div>描述信息</div>
      </div>
    </div>
  );
};

const ExtraContent: React.FC<{}> = () => (
  <div className={styles.extraContent}>
    {/* <div className={styles.statItem}>
      <Statistic title="项目数" value={56} />
    </div>
    <div className={styles.statItem}>
      <Statistic title="团队内排名" value={8} suffix="/ 24" />
    </div>
    <div className={styles.statItem}>
      <Statistic title="项目访问" value={2223} />
    </div> */}
  </div>
);

const Welcome: React.FC<WelcomeProps> = props => {
  const { currentUser } = props;

  return (
    <PageHeaderWrapper
      content={<PageHeaderContent currentUser={currentUser} />}
      extraContent={<ExtraContent />}
    >
      <Card style={{}} title="首页啦啦啦" bordered={false}>
        首页
      </Card>
    </PageHeaderWrapper>
  );
};

export default connect(({ user }: ConnectState) => ({
  currentUser: user.currentUser,
}))(Welcome);
