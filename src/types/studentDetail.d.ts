import { Moment } from 'moment';
import { StudentItem } from './studentList';

export interface EduHistoryItem {
  key?: string;
  eduDate?: string;
  orgName?: string;
  course?: string;
}

export interface FamilyMemberItem {
  key?: string;
  name?: string;
  relation?: string;
  birthday?: string;
  professor?: string;
  phone?: string;
}

export interface LiveTogetherItem {
  key?: string;
  relation?: string;
  isMain?: boolean;
  togetherTime?: string;
}

export interface MedicalDiagItem {
  key?: string;
  diagDate?: string | Moment;
  diagTool?: string;
  diagOrg?: string;
  mainObstacleType?: string;
  obstacleLevel?: string;
  secondObstacleType?: string;
  cause?: string;
  fileList?: string[];
}

export interface GrowthInfo {
  /** 妊娠初期体重 */
  pregnancyWeight?: string;
  /** 是否服用药物 */
  isTakeMedicine?: boolean;
  /** 是否抽烟酗酒 */
  isSmoke?: boolean;
  // -- 妊娠期间 --
  /** 情绪特征 */
  mood?: string;
  /** 异常生理反应 */
  unNormalResponse?: string;
  /** 睡眠质量 */
  sleepQuality?: string;
  /** 是否倒时差 */
  isJetLag?: boolean;
  /** 工业化学因素 */
  chemicalFactors?: string[];
  /** 物理因素 */
  physicalFactors?: string[];
  /** 生物因素 */
  biologicalFactors?: string[];
  /** 营养因素 */
  nutritionFactors?: string[];
  /** 不良习惯 */
  badHabits?: string[];
  // -- 分娩过程 --
  /** 分娩前 */
  childbirthBefore?: string[];
  /** 分娩时 */
  childbirthing?: string[];
  // -- 新生儿特征 --
  /** 体重 */
  babyWeight?: string;
  /** 体长 */
  babyLength?: string;
  /** 头围 */
  babyHeadCircum?: string;
  /** 窒息 */
  asphyxia?: string;
  /** 主要喂养方式 */
  mainFeed?: string;
  // -- 语言发展  --
  /** 首个词汇出现时间 */
  firstVoca?: string;
  /** 首个句子出现时间 */
  firstSentence?: string;
  // -- 动作发展 --
  /** 抬头时间 */
  lookUpTime?: string;
  /** 独立坐时间 */
  seatTime?: string;
  /** 独立站时间 */
  stationTime?: string;
  /** 独立走时间 */
  goTime?: string;
  // -- 自理能力发展 --
  /** 表达小便时间 */
  expressPeeTime?: string;
  /** 表达大便时间 */
  expressStoolTime?: string;
  /** 独立小便时间 */
  peeTime?: string;
  /** 独立大便时间 */
  stoolTime?: string;
}

export interface MedicalInfo {
  /** 遗传病史 */
  geneticSickHistory?: string;
  /** 重大疾病史 */
  majorSickHistory?: string;
  /** 是否经常生病 */
  isOftenSick?: boolean;
  /** 儿童睡眠质量 */
  childSleepQuality?: string;
  // -- 生理特殊情况 --
  /** 癫痫 */
  isEpilepsy?: boolean;
  /** 哮喘 */
  isAsthma?: boolean;
  /** 过敏信息 */
  allergyType?: string;
  /** 过敏原因 */
  allergyCause?: string;
  /** 视觉情况 */
  visionStatus?: string;
  /** 左眼视力 */
  visionLeft?: string;
  /** 右眼视力 */
  visionRight?: string;
  /** 障碍原因 */
  visionCause?: string;
  /** 听觉情况 */
  hearingStatus?: string;
  /** 左耳听力 */
  hearingLeft?: string;
  /** 右耳听力 */
  hearingRight?: string;
  /** 障碍原因 */
  hearingCause?: string;
}

export interface StudentDetailItem extends StudentItem {
  /** 昵称 */
  nickname?: string;
  /** 民族 */
  nation?: string;
  /** 血型 */
  bloodType?: string;
  /** 国籍 */
  nationality?: string;
  /** 宗教信仰 */
  religion?: string;
  /** 户籍地：省市区 */
  registPlace?: string[];
  /** 生长地：省市区 */
  growthPlace?: string[];
  /** 学籍状态 */
  schoolRollStatus?: string;
  /** 学籍号 */
  schoolRollNo?: string;
  /** 是否持残疾证 */
  hasDisableCert?: boolean;
  /** 教育经历：时间、学校/机构、学习课程 */
  eduHistory?: EduHistoryItem[];
  /** 最喜欢的玩具 */
  likeToy?: string;
  /** 最不喜欢的玩具 */
  dislikeToy?: string;
  /** 最喜欢的活动 */
  likeActivity?: string;
  /** 最不喜欢的活动 */
  dislikeActivity?: string;
  /** 最喜欢的食物 */
  likeFood?: string;
  /** 最不喜欢的食物 */
  dislikeFood?: string;
  /** 个性品质 */
  personality?: string[];
  /** 不良行为习惯 */
  badHabit?: string[];
  /** 常用表达方式 */
  expression?: string;
  /** 惯性语言 */
  language?: string;

  // -- 家庭信息 --
  /** 家庭人数 */
  familyNum?: string;
  /** 家庭住址 */
  familyAddress?: string;
  /** 家庭年收入 */
  familyIncomeYear?: string;
  /** 家庭语言环境 */
  familyLanguage?: string;
  /** 教养方式 */
  teachType?: string;
  /** 家庭成员 */
  familyMember?: FamilyMemberItem[];
  /** 共同生活者 */
  liveTogether?: LiveTogetherItem[];

  // -- 医学诊断 --
  medicalDiag?: MedicalDiagItem[];

  /** 生长史 */
  growthInfo?: GrowthInfo;

  /** 医疗信息 */
  medicalInfo?: MedicalInfo;
}
