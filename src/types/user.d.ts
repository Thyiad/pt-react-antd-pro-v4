import { CurrentUser } from '@/models/user';

export interface UserItem extends CurrentUser {
  createdAt?: string;
  schoolId?: string;
  schoolName?: string;
}

export interface UserListParams {
  schoolId?: string;
  name?: string;
  pageSize?: number;
  current?: number;
}
