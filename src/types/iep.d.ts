import { EvaCategory, EvaQuestion } from './evaluate';
import { StudentDetailItem } from './studentDetail';

export interface IEPTarget {
  // 2级分类id
  category2Id: string;
  // 3级分类id
  category3Id: string;
  // 3级分类名称
  category3Name: string;
  // 条目id
  questionId: string;
  // 条目名称
  questionName: string;
  // 是否为自定义目标
  isCustom: boolean;
}

export interface PassRateHistory {
  // 通过率
  passRate: number;
  // 日期
  date: string;
}

export interface IEP {
  // 学生id
  studentId: string;
  // 评估工具
  evaTool: string;
  // 评估领域
  categoryId: string;
  // 评估学段，目前仅在国标有效
  period: string;
  // 评估目标
  iepTargets: IEPTarget[];
  // 通过的目标id列表
  passTargetIds: string[];
  // 通过率历史记录
  passRateHistory: PassRateHistory[];
}

interface ChartDataItem {
  x: string;
  y: number;
}

export interface IEPData {
  errMsg: string;
  termIepId: string;
  // 趋势图
  trend: ChartDataItem[];
  // 雷达图
  radar: ChartDataItem[];
  // iep目标
  iepTargets: IEPTarget[];
  // 已通过的目标id列表
  passTargetIds: string[];
  // 分类列表，该领域下面的所有分类
  categoryList: EvaCategory[];
  // 评估中，所有未通过的条目
  unpassQuestionList: EvaQuestion[];
}

export interface Level2Report {
  category2Id: string;
  category2Name: string;
  iepTargets: IEPTarget[];
}

export interface Level1Report {
  category1Id: string;
  category1Name: string;
  bar: ChartDataItem[];
  level2Report: Level2Report[];
}

export interface IepReportData {
  errMsg?: string;
  studentInfo: StudentDetailItem;
  国标: Level1Report[];
}

export interface IepReportRecordItem extends BaseModel {
  // 学生id
  studentId: string;
  // 报告信息
  reportData: any;
  // 存档人
  createUser: string;
  // 描述
  desc: tring;
}
