export interface StudentItem extends BaseModel {
  schoolId?: string;
  clsId?: string;
  clsName?: string;
  name?: string;
  avatar?: string;
  birthday?: string;
  sex?: string;
  cardNo?: string;
  obstacleType?: string;
  placement?: string;
  [key: string]: string;
}

export interface StudentListParams {
  schoolId?: string;
  clsId?: string | string[];
  specialStuIdList?: string[];
  name?: string;
  sex?: string;
  obstacleType?: string;
  placement?: string;
  pageSize?: number;
  current?: number;
}
