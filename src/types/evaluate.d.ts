export interface EvaCategory extends BaseModel {
  /**
   * 所属评估工具
   */
  evaTool?: string;
  /**
   * 名称
   */
  name?: string;
  /**
   * 第几级，从1开始
   */
  level?: number;
  /**
   * 父节点 id
   */
  parentId?: string;
  /**
   * 排序权重
   */
  sortOrder?: number;
  // 目前结论语只有level2有用到
  /**
   * 通过率好的结论语
   */
  resultA?: string;
  /**
   * 通过率一般的结论语
   */
  resultB?: string;
  /**
   * 通过率稍差的结论语
   */
  resultC?: string;
  /**
   * 通过率较差的结论语
   */
  resultD?: string;
}

export interface EvaQuestion extends BaseModel {
  /**
   * 所属评估工具
   */
  evaTool?: string;
  /**
   * 所属分类
   */
  categoryId?: string;
  /**
   * 条目名称
   */
  name?: string;
  /**
   * 条目提示
   */
  tip?: string;
  /**
   * 所归属的学段
   */
  periods?: string[];
  /**
   * 排序权重
   */
  sortOrder?: number;
  /**
   * 选项A
   */
  optionA?: string;
  /**
   * 选项B
   */
  optionB?: string;
  /**
   * 选项C
   */
  optionC?: string;
  /**
   * 选项D
   */
  optionD?: string;
  /**
   * 课程名称
   * 目前只有国标使用到
   */
  courseName?: string;
}

export interface EvaRecord extends BaseModel {
  evaTool?: string;
  studentId?: string;
  categoryId?: string;
  period?: string;
  teacherId?: string;
  teacherName?: string;
  ansAList?: string[];
  ansBList?: string[];
  ansCList?: string[];
  ansDList?: string[];
  // 是否草稿，预留给草稿功能使用的
  isDraft?: boolean;
}

interface SubCateQuestion {
  questionName: string;
  score: string;
  rowSpan: number;
}

interface SubCate {
  cateName: string;
  questionList: SubCateQuestion[];
}

interface BarItem {
  x: string;
  y: number;
}

export interface EvaReport {
  // 评估学段
  evaPeriod: string;
  // 评估日期
  evaDate: string;
  // 评估人
  evaUserName: string;
  // 评估分类名称
  categoryName: string;
  // 柱状图
  bar: BarItem[];
  // 整体分析
  analyseResult: string[];
  // 子领域得分情况
  subCateList: SubCate[];
}
