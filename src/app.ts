export const dva = {
  config: {
    onError(err: any) {
      err.preventDefault();
    },
  },
};

export function render(oldRender: any) {
  oldRender();
}
