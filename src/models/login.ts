import { Reducer } from 'redux';
import { Effect } from 'dva';
import { stringify } from 'querystring';
import { router } from 'umi';

import { fakeAccountLogin } from '@/services/login';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import * as api from '@/constants/api';
import Cookies from 'js-cookie';
import { LOGIN_COOKIE_KEY, LOGIN_ROLE_KEY } from '@/constants/index';

export interface StateType {}

export interface LoginModelType {
  namespace: string;
  state: StateType;
  effects: {
    login: Effect;
    logout: Effect;
  };
  reducers: {};
}

const Model: LoginModelType = {
  namespace: 'login',

  state: {},

  effects: {
    login() {
      Cookies.remove(LOGIN_COOKIE_KEY);
      Cookies.remove(LOGIN_ROLE_KEY);
      const nowPath = window.location.pathname + window.location.search + window.location.hash;
      router.push(`/user/login?target=${encodeURIComponent(nowPath)}`);
    },

    logout() {
      Cookies.remove(LOGIN_COOKIE_KEY);
      Cookies.remove(LOGIN_ROLE_KEY);
      const homePath = '/';
      window.location.href = `/user/login?target=${encodeURIComponent(homePath)}`;
    },
  },

  reducers: {},
};

export default Model;
