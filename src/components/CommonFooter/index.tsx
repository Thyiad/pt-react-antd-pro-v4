import React from 'react';
import styles from './index.less';

const CommonFooter: React.FC = () => (
  <div className={styles.footer}>
    2012 - {new Date().getFullYear()} ©{' '}
    <a href="http://www.thyiad.top" target="_blank" rel="noopener noreferrer">
      Thyiad
    </a>
    . All rights reserved. &nbsp;
  </div>
);

export default CommonFooter;
