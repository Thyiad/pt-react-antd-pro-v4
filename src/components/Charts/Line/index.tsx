import React from 'react';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util,
} from 'bizcharts';

interface LineProps {
  height?: number;
  data: {
    x: string;
    y: number;
  }[];
  forceFit?: boolean;
  scale?: {
    [key: string]: any;
  };
  color?: string;
  toolTipFun?: (x: string, y: string) => { name: string; value: string };
}

const Line: React.FC<LineProps> = props => {
  const {
    height = 300,
    forceFit = true,
    data,
    color = 'rgba(24, 144, 255, 0.85)',
    scale = {},
    toolTipFun = (x: string, y: string) => ({
      name: x,
      value: y,
    }),
  } = props;

  const cols = {
    x: {
      //   range: [0, 1]
      type: 'cat',
    },
    y: {
      min: 0,
      max: 100,
    },
    ...scale,
  };

  const tooltip: [string, (...args: any[]) => { name?: string; value: string }] = [
    'x*y',
    toolTipFun,
  ];
  return (
    <div style={{ height }}>
      <Chart height={height} data={data} scale={cols} forceFit>
        <Axis name="x" />
        <Axis name="y" />
        <Tooltip
          crosshairs={{
            type: 'y',
          }}
        />
        <Geom type="line" position="x*y" size={2} tooltip={tooltip} />
        <Geom
          type="point"
          position="x*y"
          size={4}
          shape="circle"
          style={{
            stroke: '#fff',
            lineWidth: 1,
          }}
        />
      </Chart>
    </div>
  );
};

export default Line;
