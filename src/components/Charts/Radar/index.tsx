import React from 'react';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util,
} from 'bizcharts';
import DataSet from '@antv/data-set';

const { DataView } = DataSet;

interface RadarProps {
  height?: number;
  data: {
    x: string;
    y: number;
  }[];
  forceFit?: boolean;
  scale?: {
    [key: string]: any;
  };
  tooltipFun?: (...args: any[]) => { name?: string; value: string };
}

const Radar: React.FC<RadarProps> = props => {
  const {
    height = 300,
    forceFit = true,
    scale = {},
    data,
    tooltipFun = (valueType: string, score: string) => ({
      name: valueType,
      value: score,
    }),
  } = props;

  const dv = new DataView().source(data);
  dv.transform({
    type: 'fold',
    fields: ['y'],
    // 展开字段集
    key: 'valueType',
    // key字段
    value: 'score', // value字段
  });
  const cols = {
    score: {
      min: 0,
      max: 100,
    },
    ...scale,
  };
  const tooltip: [string, (...args: any[]) => { name?: string; value: string }] = [
    'valueType*score',
    tooltipFun,
  ];
  return (
    <div style={{ height }}>
      <Chart height={height} data={dv} scale={cols} forceFit={forceFit}>
        <Coord type="polar" radius={0.8} />
        <Axis
          name="x"
          line={null}
          tickLine={null}
          grid={{
            lineStyle: {
              lineDash: null,
            },
            hideFirstLine: false,
          }}
        />
        <Tooltip />
        <Axis
          name="score"
          line={null}
          tickLine={null}
          grid={{
            type: 'polygon',
            lineStyle: {
              lineDash: null,
            },
            alternateColor: 'rgba(0, 0, 0, 0.04)',
          }}
        />
        <Geom type="line" position="x*score" color="valueType" size={2} tooltip={tooltip} />
        <Geom
          tooltip={tooltip}
          type="point"
          position="x*score"
          color="valueType"
          shape="circle"
          size={4}
          style={{
            stroke: '#fff',
            lineWidth: 1,
            fillOpacity: 1,
          }}
        />
      </Chart>
    </div>
  );
};

export default Radar;
