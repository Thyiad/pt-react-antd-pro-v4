/**
 * 性别列表
 */
export const sexList = [
  { value: '1', label: '男' },
  { value: '2', label: '女' },
];

/**
 * 角色列表
 */
export const roleList = [
  { value: 'admin', label: '超级管理员' },
  { value: 'user', label: '普通用户' },
];
