/**
 * token的cookie名
 */
export const LOGIN_COOKIE_KEY = 'loginCookieKey';
export const LOGIN_ROLE_KEY = 'loginRoleKey';

/**
 * ajax请求的返回code对象
 */
export const RES_CODE = {
  /**
   * 业务逻辑成功
   */
  OK: 2000,
  /**
   * 业务逻辑失败
   */
  ERROR: 3000,
  /**
   * token为空或非法
   */
  TOKEN_BAD: 4000,
};

/**
 * 角色对象
 */
export const roles = {
  admin: 'admin', // 系统管理员
  user: 'user', // 用户
};

/**
 * 默认头像 url
 */
export const defaultAvatarUrl =
  'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png';
