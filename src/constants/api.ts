const env = process.env.UMI_ENV;

let host = '';

if (env === 'test') {
  host = 'https://easymock.thyiad.top/mock/5fabb662f398e40020f988dd/boilerplate';
  // host = 'http://localhost:5001';
}

export const HOST = host;

export const LOGIN = `${HOST}/login`;
export const GET_USERINFO = `${HOST}/getUserInfo`;
export const GET_LIST = `${HOST}/tableList`;
export const COMMON_OK = `${HOST}/commonOK`;
export const UPDATE_PASSWORD = `${HOST}/commonOK`;
export const UPDATE_PROFILE = `${HOST}/coomonOK`;
