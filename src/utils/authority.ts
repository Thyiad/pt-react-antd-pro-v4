import { LOGIN_ROLE_KEY } from '@/constants/index';
import Cookies from 'js-cookie';
import { getBaseHost } from '@/utils/utils';
import { reloadAuthorized } from './Authorized';

/**
 * 从cookie中获取当前角色，暂时不考虑多个角色
 * @param str
 */
export function getAuthority(str?: string): string | undefined {
  let authorityString = str;
  if (typeof str === 'undefined') {
    authorityString = Cookies.get(LOGIN_ROLE_KEY);
  }
  return authorityString;
}

/**
 * 将当前角色存到cookie中，暂时不考虑多个角色
 * @param authority
 */
export function setAuthority(authority: string): void {
  const baseHost = getBaseHost();
  Cookies.set(LOGIN_ROLE_KEY, authority, {
    domain: baseHost,
  });
  reloadAuthorized();
}
